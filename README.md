docker build \
    -t ttl.sh/express-app:2h \
    --build-arg BUILD_NAME=express-app \
    --build-arg BUILD_VERSION=$(git branch --show-current)-$(git rev-parse --short HEAD) .



docker run --name express-app -p 8080:3000  ttl.sh/express-app:2h