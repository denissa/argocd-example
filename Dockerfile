FROM node:20.8.0-slim

ARG BUILD_VERSION
ARG BUILD_NAME

ENV PORT=3000
ENV BUILD_VERSION=$BUILD_VERSION
ENV BUILD_NAME=$BUILD_NAME

WORKDIR /app

COPY package.json  /app/

RUN npm install --production

COPY . /app/

EXPOSE $PORT

CMD ["node", "index.js"]