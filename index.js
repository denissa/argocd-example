const http = require('http');

const port = process.env.PORT || 3000;
const buildVersion = process.env.BUILD_VERSION || 'Unknown';
const buildName = process.env.BUILD_NAME || 'Unknown';

const server = http.createServer((req, res) => {
  if (req.url === '/') {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(`Hi! My name is: ${buildName}`);
  } else if (req.url === '/readiness') {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Ok');
  } else if (req.url === '/liveness') {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Ok');
  } else if (req.url === '/version') {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(`Build version: ${buildVersion}`);
  } else {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Not Found');
  }
});

const handleShutdown = () => {
  console.log('Server shutting down...');
  server.close(() => {
    console.log('Server successfully shut down.');
    process.exit(0);
  });
};

process.on('SIGTERM', handleShutdown);
process.on('SIGINT', handleShutdown);

server.listen(port, () => {
  console.log(`Server running on port ${port}`);
  console.log(`Build version: ${buildVersion}`);
});